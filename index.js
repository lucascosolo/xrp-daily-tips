const Twit = require('twit');
const fs = require('fs');
const emoji = require('node-emoji')

const config = require('./config.json');

var bot = new Twit(config);

var tweet = fs.readFileSync('./tweet.txt').toString();
tweet = emoji.emojify(tweet);

var data = fs.existsSync('./data.json') ? require('./data.json') : {
    rewarded_users: new Array()
};

function sendTweet() {
    bot.post('statuses/update', { status: tweet }, function (err, dat, res) {
        if (err) console.error(err);
        else {
            data = {
                tweet_id: dat.id_str,
                tweet_date: Date.now(),
                rewarded_users: new Array()
            };
            fs.writeFile('./data.json', JSON.stringify(data));
        }
    });
}

function give(replies, index) {
    if (index >= replies.length) {
        console.log('Done rewarding!');
        fs.writeFileSync('./data.json', JSON.stringify(data));
        return;
    }
    var reply = replies[index];
    console.log(`Checking reply by ${reply.user.screen_name} (${reply.user.id_str})`);
    if (!reply.entities.user_mentions || reply.entities.user_mentions.length < 1) {
        console.log(" Reply didn't tag anyone.");
    } else if (reply.in_reply_to_status_id_str !== data.tweet_id) {
        console.log(' Reply was to the wrong tweet.');
    } else if (data.rewarded_users.indexOf(reply.user.id_str) > -1) {
        console.log(' Reply has already been rewarded.');
    } else {
        data.rewarded_users.push(reply.user.id_str);
        bot.post('statuses/update', {
            status: `@${reply.user.screen_name} +0.02 @xrptipbot`,
            in_reply_to_status_id: reply.id_str
        }, function(err, dat, res) {
            if (err) console.error(err);
            else {
                bot.post('favorites/create', { id: reply.id_str }, function (_, __, ___) {});
                console.log('Sent!');
            }
            setTimeout(function() {
                give(replies, index + 1);
            }, 1000 * (Math.floor(Math.random() * 10) + 8));
        });
        return;
    }
    give(replies, index + 1);
}

function sendRewards() {
    console.log("Rewarding...");
    console.log(`Hours between dates: ${(Date.now() - data.tweet_date) / 1000 / 60 / 60}`)
    if (!data.tweet_id || Date.now() - data.tweet_date > 1000 * 60 * 60 * 23.833) {
        console.log("Sending new tweet!");
        sendTweet();
        setTimeout(sendRewards, 1000 * 60 * 5);
        return;
    }
    if (data.rewarded_users.length >= 40) {
        console.log("No reward slots left.");
        setTimeout(sendRewards, 1000 * 60 * 20);
        return;
    }
    bot.get('search/tweets', { q: 'to:LucasCosolo', since_id: data.tweet_id }, function(err, data, res) {
        if (err) console.error(err);
        else {
            console.log(`Found ${data.statuses.length} replies!`);
            give(data.statuses, 0);
        }
        setTimeout(sendRewards, 1000 * 60 * 5);
    });
}

sendRewards();
